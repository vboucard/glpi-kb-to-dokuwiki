dnf install epel-release -y
dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
dnf install glibc-all-langpacks -y
crb enable -y
dnf module enable php:remi-8.2 -y

dnf install -y unzip curl wget lsof bind-utils bash-completion mlocate bzip2 tcpdump
dnf install -y policycoreutils-python-utils tree
dnf install -y php php-gd php-mbstring php-intl php-xml php-json php-ldap

DOKU_DIR="/var/www/html/dokuwiki"
TPL_DIR="$DOKU_DIR/lib/tpl"
PLUGINS_DIR="$DOKU_DIR/lib/plugins"

URL="https://github.com/dokuwiki/dokuwiki/releases/download/release-2023-04-04a/dokuwiki-2023-04-04a.tgz"
DEST="dokuwiki.tar.gz"

URL2="https://github.com/selfthinker/dokuwiki_plugin_wrap/archive/stable.zip"
DEST2="wrap.zip"

URL3="https://github.com/michitux/dokuwiki-plugin-move/zipball/master"
DEST3="move.zip"

URL4="https://github.com/turnermm/ckgedit/archive/current.zip"
DEST4="ckgedit.zip"

if [[ ! -f $DEST ]]
then
    sed -i "/post_max_size =/ s/= .*/= 50M/" /etc/php.ini
    sed -i "/upload_max_filesize =/ s/= .*/= 50M/" /etc/php.ini
    systemctl restart php-fpm
    
    wget $URL -O $DEST
    tar xzf $DEST --directory=/var/www/html
    mv $DOKU_DIR-* $DOKU_DIR

    wget $URL2 -O $DEST2
    unzip $DEST2 -d $TPL_DIR >/dev/null
    mv $TPL_DIR/dokuwiki_plugin_wrap-stable $TPL_DIR/wrap

    wget $URL3 -O $DEST3
    unzip $DEST3 -d $PLUGINS_DIR >/dev/null
    mv $PLUGINS_DIR/michitux* $PLUGINS_DIR/move

    wget $URL4 -O $DEST4
    unzip $DEST4 -d $PLUGINS_DIR >/dev/null
    mv $PLUGINS_DIR/ckgedit* $PLUGINS_DIR/ckgedit
fi

if [[ ! -f /etc/httpd/conf.d/dokuwiki.conf ]]
then
    cat << EOF > /etc/httpd/conf.d/dokuwiki.conf
<VirtualHost *:80>
  DocumentRoot /var/www/html/dokuwiki/

    <Directory /var/www/html/dokuwiki/>
        Require all granted
        AllowOverride All
        Options FollowSymLinks MultiViews

        <IfModule mod_dav.c>
            Dav off
        </IfModule>

    </Directory>
    <LocationMatch "/(data|conf|bin|inc|vendor)/">
        Order allow,deny
        Deny from all
        Satisfy All
    </LocationMatch>
</VirtualHost>
EOF
fi

# dokuwiki n'a pas été installé
if [[ -f $DOKU_DIR/install.php ]]
then
    rm -f $DOKU_DIR/install.php
    cp /vagrant/*.php $DOKU_DIR/conf/
fi

semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/dokuwiki/conf(/.*)?"
semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/dokuwiki/data(/.*)?"
restorecon -R /var/www/html/dokuwiki/conf
restorecon -R /var/www/html/dokuwiki/data

semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/dokuwiki/lib/plugins(/.*)?"
restorecon -R /var/www/html/dokuwiki/lib/plugins
semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/dokuwiki/lib/tpl(/.*)?"
restorecon -R /var/www/html/dokuwiki/lib/tpl

chown -R apache: $DOKU_DIR
systemctl enable --now httpd
systemctl enable --now firewalld
firewall-cmd --permanent --add-service=http
firewall-cmd --reload