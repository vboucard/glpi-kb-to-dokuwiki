<?php
/*
 * Dokuwiki's Main Configuration File - Local Settings
 * Auto-generated by config plugin
 * Run for user: admin
 * Date: Sat, 28 Oct 2023 14:34:37 +0000
 */

$conf['title'] = 'Test de migration';
$conf['lang'] = 'fr';
$conf['license'] = 'cc-by-sa';
$conf['basedir'] = '/';
$conf['useacl'] = 1;
$conf['superuser'] = '@admin';
$conf['disableactions'] = 'register,resendpwd,profile,profile_delete';
$conf['plugin']['authldap']['attributes'] = array();
